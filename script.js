// Handle form submit
document.getElementById('post-form').addEventListener('submit', async (e) => {

    // Get form data
    const text = document.getElementById('post-text').value; 
    const image = document.getElementById('post-image').files[0];
    const link = document.getElementById('post-link').value;
    const attachment = document.getElementById('post-attachment').files[0];
  
    // Create post object
    const post = {
      text, 
      image,
      link,
      attachment
    };
  
    // Save post to database
    await savePost(post);
  
    // Prevent default form submit
    e.preventDefault(); 
  });
  
  // Create DOM element for a post
  function createPostElement(post) {
  
    const postEl = document.createElement('div'); 
    postEl.classList.add('post');
  
    if (post.text) {
      // Create paragraph for text
      const textEl = document.createElement('p');
      textEl.textContent = post.text;
      postEl.appendChild(textEl);
    }
  
    if (post.image) {
      // Create img element for image
      const imgEl = document.createElement('img');
      imgEl.src = post.image;
      postEl.appendChild(imgEl);
    }
  
    if (post.link) {
      // Create anchor for link
      const linkEl = document.createElement('a');
      linkEl.href = post.link;
      linkEl.textContent = post.link;
      postEl.appendChild(linkEl);
    }
  
    if (post.attachment) {
      // Create anchor for attachment
      const attachEl = document.createElement('a');
      attachEl.href = post.attachment;
      attachEl.textContent = 'Attachment';
      postEl.appendChild(attachEl);
    }
  
    return postEl;
  
  }
  
  // Schema for post 
  const postSchema = new mongoose.Schema({
    text: String,
    image: String,
    link: String,
    attachment: String
  });
  
  const Post = mongoose.model('Post', postSchema);
  
  // Insert post into MongoDB
  async function savePost(post) {
    
    let newPost = new Post(post);
    await newPost.save();
  
  } 
  
  // Get posts from MongoDB
  async function getPosts() {
    
    return await Post.find(); 
  
  }
  
  // Connect to MongoDB
  try {
  
    await mongoose.connect(
      'mongodb+srv://<username>:<password>@<cluster>.mongodb.net/?retryWrites=true&w=majority'
    );
  
    // Get posts from database
    const posts = await getPosts();
  
    // Display each post
    posts.forEach(post => {
  
      const postEl = createPostElement(post);
  
      document.getElementById('posts').appendChild(postEl);
  
    });
  
  } catch (error) {
    
    console.log(error);
  
  }
  